
export function setHostHeader(data: Buffer, host: string): Buffer {
  let dataStr = data.toString("utf-8");
  dataStr = dataStr.replace(/^Host:.+$/m, `Host: ${host}`);
  return Buffer.from(dataStr, "utf-8");
}
