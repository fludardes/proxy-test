import { Server } from "net";

import { PROXY_HOST, PROXY_PORT } from "./conf";
import log from "./log";
import process from "./process";

export function run() {
  const server = new Server(process);

  log.info("Starting proxy server...");

  server.listen(PROXY_PORT, PROXY_HOST, (err: Error) => {
    if (err) {
      log.error("Server error:", err.message);
    }

    log.info(`Started proxy server on ${PROXY_HOST}:${PROXY_PORT}`);
  });

  server.on("error", (err) => {
    log.error("Server error:", err.message);
  });
}
