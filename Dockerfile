FROM node:8
# Set work directory to /proxy
WORKDIR /proxy
# Copy app
ADD . /proxy
RUN yarn
# Listen address inside Docker
ENV PROXY_HOST "0.0.0.0"
# Outer port
EXPOSE 80
# Start
ENTRYPOINT ["yarn", "start"]
